#!/bin/sh

ring_color=73938e
transparent=00000000
font='Fira Mono'
font_color=2e3440ff

i3lock --nofork                 	\
    --ignore-empty-password		\
    --pass-volume-keys 			\
    \
    --radius="270" 			\
    --ring-width="13" 			\
    --ring-color="${ring_color}aa"	\
    --ringver-color="${ring_color}ff"	\
    --ringwrong-color="${ring_color}ff"	\
    --inside-color=$transparent 		\
    --insidever-color=$transparent 	\
    --separator-color=$transparent 	\
    --insidewrong-color=ea4950aa 	\
    --bshl-color=f6ce89aa		\
    --keyhl-color=778d87ff		\
    --line-uses-inside 			\
    \
    --clock				\
    --time-str="%H:%M"			\
    --time-font="$font"			\
    --time-size=100			\
    --time-pos="1500:900"		\
    --time-color=$font_color		\
    \
    --date-str="%A, %d %B"		\
    --date-font="$font"			\
    --date-color=$font_color		\
    --date-size=45			\
    --date-pos="tx:979"			\
    \
    --verif-text=""			\
    --wrong-text=""			\
    --wrong-font="$font"		\
    --wrong-color=$font_color 		\
    --wrong-size=45			\
    \
    --image=/home/tonyk/.config/i3/i3lock_image.png
