#!/bin/bash

a=$(setxkbmap -query | awk '/layout/{print $2}')
[[ "$a" == us ]] && b="br" || b="us"
setxkbmap $b
